<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use Redirect;
class UsuariosController extends Controller
{
    public function  index(){
        //puxa os dados do banco
        $usuarios = Usuario::get();
        return view('usuarios.listar', ['usuarios' => $usuarios]);
    }

    public function new(){
        return view('usuarios.form');
    }

    public function add( Request $request ){
         //validação
      $regras = [
        'name'  => 'required|max:40',      
        'email' => 'email|required'
      ];
      $retorno_usuario = [
        'required'  =>'O campo nome deve ser preechido.',
        'name.max'  => 'O nome não pode ter mais que 40 caracteres.',
        'email'     => 'O campo email não é válido.'
      ];
      // validando
      $request->validate($regras, $retorno_usuario);

        $usuario = new Usuario;
        $usuario =  $usuario::create($request->all());  
        return Redirect::to('/usuarios');
    }

    public function edit($id){
        $usuario = Usuario::findOrFail($id);
        return view('usuarios.form', ['usuario'=>$usuario]);
    }

    public function update($id, Request $request){
        //validação
      $regras = [
        'name'  => 'required|max:40',      
        'email' => 'email|required'
      ];
      $retorno_usuario = [
        'required'  =>'O campo nome deve ser preechido.',
        'name.max'  => 'O nome não pode ter mais que 40 caracteres.',
        'email'     => 'O campo email não é válido.'
      ];

      // validando: caso houver insconssistências não irá adiante, ou seja, não irá gravar as alterações se houver.
      $request->validate($regras, $retorno_usuario);

        $usuario = Usuario::findOrFail($id);
        $usuario -> update($request->all());
        return Redirect::to('/usuarios');
    }

    public function delete($id){
        $usuario = Usuario::findOrFail($id);
        $usuario -> delete($id);
        return Redirect::to('/usuarios');
    }
}
