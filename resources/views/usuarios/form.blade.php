@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <p align="center">
                        {{ __(':.  Cadastro de Usuários    .:') }}</div>
                    </p>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>                  
                    @endif
                  
                @if(Request::is('*/edit'))                
                  <!-- formulário  Atualizar-->
                  <form action="{{ url('usuarios/update')}}/{{$usuario->id}}" method="post">
                  @csrf
                    <div class="row mb-3">
                        <label for="nome" class="col-sm-2 col-form-label">Nome</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" value="{{ $usuario->name}}">
                            
                        </div>
                    </div>
                    
                    <div class="row mb-3">
                        <label for="email" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                           <input type="text" name="email" class="form-control" value="{{ $usuario->email}}">
                           
                        </div>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Atualizar</button>
                   </form>
                
                @else
                   <!--fecha o form-->
                  <!-- formulário Cadastrar -->
                  <form action={{ url('usuarios/add')}} method="post">
                  @csrf
                    <div class="row mb-3">
                        <label for="nome" class="col-sm-2 col-form-label">Nome</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" id="nome"  value="{{old('name')}}">
                             {{ $errors->has('name') ? $errors->first('name') : ''}}
                        </div>
                    </div>
                    
                    <div class="row mb-3">
                        <label for="email" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                           <input type="text" name="email" class="form-control" id="email"  value="{{old('email')}}">
                            {{ $errors->has('email') ? $errors->first('email') : ''}}
                        </div>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                   </form>
                   <!--fecha o form -->
                 @endif

                 </div>
            </div>
        </div>
    </div>
@endsection
