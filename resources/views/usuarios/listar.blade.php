@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <p align="center"><b>{{ __('.:   Listar Usuários     .:') }}</b></p>                    
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nome</th>
                                <th scope="col">E-mail</th>
                                <th scope="col">Editar</th>
                                <th scope="col">Deletar</th>
                            </tr>
                        </thead>

                        @foreach ($usuarios as $u)                        
                            <tbody>
                                <tr>        
                                    <th scope="row">{{ $u -> id}}</th>                                                            
                                    <td>{{ $u -> name }}</td>
                                    <td>{{ $u -> email }}</td>
                                    <td><a href="usuarios/{{$u->id}}/edit" class="btn btn-info">Editar</a></td>
                                    <td>
                                        <form action="usuarios/delete/{{ $u->id}}" method='post'>
                                            @csrf
                                            @method('delete')
                                            <button class="btn btn-danger">Deletar</button>
                                        </form>
                                    </td>
                                    <!--
                                       outra forma de fazer caso use a rota get
                                    <td><a href="usuarios/{{$u->id}}/delete" class="btn btn-danger">Deletar</a></td>
                                    
                                    -->
                                </tr>                                
                            </tbody>
                        @endforeach
                    </table>                                 
                    <a href="{{ url('/home')}}">Voltar</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
