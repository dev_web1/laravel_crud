@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <p align="center">
                        {{ __(':.   BookSystem    .:') }}
                        <br>
                        {{ __('Seja Bem-vindo!') }}
                    </p>
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                    <br>
                    <a href={{ url('usuarios')}}>Listar Usuários</a>
                    <br>
                    <a href={{ url('usuarios/form')}}>Cadastrar</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
